# contour7350: Glucose for Linux

This project demonstrates the USB communications protocol for the Contour Next
glucose meter: vendor ID `0x1a79`, product ID `0x7350`. Similar meters might
also work, but you will need to modify this software.

## WARNING

This software documents a communications protocol—nothing more.

Although it may be capable of downloading and displaying glucose test results,
it has not been approved or certified for that or *any* other purpose. The
program is the result of reverse-engineering and was made *without the approval*
of the manufacturer. Accordingly, this program might:

  * Display erroneous results
  * Cause your meter to malfunction
  * Cause your meter to display or record inaccurate results—maybe permanently
  * Erase, tamper with, or destroy the software or firmware on your meter
  * Render your meter *permanently inoperative*

If you are unwilling to take these risks, then don't use this software.

This program is distributed under the terms of the [GNU GPLv2](LICENSE.md). THIS
PROGRAM IS PROVIDED “AS IS” WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR
IMPLIED, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.

Nothing in this program constitutes medical advice. Please direct any questions
about your health to a qualified physician.

## Building

Install some development tools, including a compiler. No other libraries are
required.

```shell
sudo yum groupinstall "Development Tools"   # CentOS-like
                                            # -OR-
sudo apt install build-essential            # Debian-like
```

Then run `make`.

Normally, root access is required to connect to raw USB devices. To avoid
running this program as root, install the udev rules in `udev/`. These rules
grant user access to both the hid and hidraw devices used by your meter. Other
devices are unaffected.

```shell
sudo cp udev/*.rules /etc/udev/rules.d/
sudo udevadm control --reload && udevadm trigger
```

## Running

Connect your meter, and then run something like

```shell
./glucose -f csv -o somefile.csv
```

The `-d` option can be used to specify a device file for your meter, such as
`/dev/hidraw0`.

Unplug your meter after the program exits.

## Credits

Forked from https://github.com/CodeMonkeySteve/contour-glucose.
