/*
 * Copyright (C) 2012 Timo Kokkonen <timo.t.kokkonen@iki.fi>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <linux/hidraw.h>
#include <errno.h>
#include <string.h>
#include <fcntl.h>
#include <dirent.h>

#include "hiddev.h"
#include "utils.h"

#define PRINT_FIELD(level, field) trace(level, #field ": %04x\n", field)

#define HIDDEV_PATH "/dev/"
#define HIDDEV_NAME "hidraw"

int hiddev_read(unsigned char *data, int bufsize, int fd)
{
	int error = 0;
	int n = read(fd, data, bufsize);
	if (n < 0)
	{
		error = errno;
		printf("Error in read: %s\n", strerror(error));
	}

	return n;
}

int hiddev_write(const unsigned char data[64], int fd , int usage_code)
{
	int error = 0;
	int n = write(fd, data, 64);
	if (n == 64)
		return 0;

	error = errno;
  printf("Error in write: %s\n", strerror(error));
	return n;
}

static int _hiddev_open(const char *device_path, int *usage_code,
			int vendor_id, int product_id)
{
	struct hidraw_devinfo device_info;
	int ret, error;
	int fd;

	trace(3, "Opening device %s\n", device_path);
	fd = ret = open(device_path, O_RDWR);

	if (fd < 0)
		goto err;

	memset(&device_info, 0, sizeof(device_info));
	ret = ioctl(fd, HIDIOCGRAWINFO, &device_info);
	if (ret < 0)
		goto err_close;

	if (product_id && vendor_id &&
		  (product_id != device_info.product ||
			 vendor_id != device_info.vendor)) {
		trace(3, "Vendor and product IDs don't match\n");
		ret = -1;
		goto err_close;
	}

	PRINT_FIELD(3, device_info.bustype);
	PRINT_FIELD(3, device_info.vendor);
	PRINT_FIELD(3, device_info.product);

	*usage_code = 0;

	return fd;

err_close:
	close(fd);
err:
	error = errno;
	if (error)
		printf("Error opening device %s: %s\n", device_path,
			strerror(error));
	return ret;
}

int hiddev_open(const char *device_path, int *usage_code)
{
	return _hiddev_open(device_path, usage_code, 0, 0);
}

int hiddev_open_by_id(int vendor_id, int product_id, int *usage_code)
{
	struct dirent *dirent;
	DIR *dir;
	int error, fd;
	char path[256];

	dir = opendir(HIDDEV_PATH);
	if (dir == NULL) {
		error = errno;
		trace(4, "Failed to open directory %s: %s\n", HIDDEV_PATH,
			strerror(error));
		return -error;
	}

	while (1) {
		dirent = readdir(dir);
		if (dirent == NULL)
			break;

		if (strncmp(dirent->d_name, HIDDEV_NAME, sizeof(HIDDEV_NAME) - 1))
			continue;

		path[0] = 0;
		strncat(path, HIDDEV_PATH, sizeof(path) - 1);
		strncat(path, dirent->d_name, sizeof(path) - 1);

		fd = _hiddev_open(path, usage_code, vendor_id, product_id);
		if (fd < 0)
			continue;

		closedir(dir);
		return fd;
	}

	if (errno) {
		error = errno;
		trace(0, "Error reading directory %s: %s\n", HIDDEV_PATH,
			strerror(error));
	  closedir(dir);
		return -error;
	}

	trace(0, "Canno't find any mathing hiddev devices\n");
	closedir(dir);
	return -1;
}
